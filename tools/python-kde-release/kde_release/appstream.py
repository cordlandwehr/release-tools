# Copyright 2015, 2019 Jonathan Riddell <jr@jriddell.org>
# Copyright 2017, 2019 Adrian Chaves <adrian@chaves.io>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from glob import glob
from os import chdir, path
from subprocess import run

from click import echo, ClickException

from kde_release.configuration import CONFIGURATION
from kde_release.environment import KDE_RELEASE_DIR
from kde_release.modules import update_modules


APPSTREAM_UPDATER = CONFIGURATION['DEFAULT']['APPSTREAM_UPDATER']


def add_version_to_appstream(directory, version, date, branch):
    appstream_files = glob(directory + '/**/*appdata.xml', recursive=True)
    appstream_files += glob(directory + '/**/*metainfo.xml', recursive=True)
    for appstream_file in appstream_files:
        date_string = date.strftime('%A, %-d %B %Y')
        result = run([APPSTREAM_UPDATER,
                      "--version", version,
                      "--datestring", date_string,
                      "--releases-to-show", "4",
                      appstream_file])
        result.check_returncode()
    if len(appstream_files) > 0:
        chdir(directory)
        result = run(['git', 'commit', '-a', '-m', 'GIT_SILENT Update Appstream for new release'])
        result.check_returncode()
        result = run(['git', 'push'])
        result.check_returncode()
        result = run(['git', 'checkout', 'master'])
        result.check_returncode()
        result = run(['git', 'cherry-pick', '-x', branch])
        result.check_returncode()
        result = run(['git', 'push'])
        result.check_returncode()


def add_versions(srcdir, date, verbose, dry, clone, hide_skipped):
    with (KDE_RELEASE_DIR / 'version').open() as version_file:
        global_version = version_file.readline().rstrip()
    if int(global_version.split('.')[2]) > 50:
        raise ClickException(
            f'Version number {global_version} indicates this is a testing '
            f'release so not adding versions to Appstream files')

    if not path.isfile(APPSTREAM_UPDATER):
        raise ClickException(
            f'Can not find appstream-metainfo-release-update executable file '
            f'at {APPSTREAM_UPDATER}.\nGet it from git '
            f'invent:sysadmin/appstream-metainfo-release-update')

    kwargs = {'clone': clone,
              'log_missing_versions': not hide_skipped}
    for directory, product, version, branch in update_modules(srcdir, **kwargs):
        if dry:
            echo(f'{product}\n'
                 f'\t(would have added {version})')
            continue
        try:
            add_version_to_appstream(directory, version, date, branch)
        except Exception as error:
            echo(f'{product}\n'
                 f'\t(would have added {version})\n'
                 f'\t\t(error: {repr(error)})')
        else:
            echo(f'{product}\n'
                 f'\t(added {version})')
