#!/bin/bash

function get_repo_path()
{
    echo $(curl -s https://projects.kde.org/api/v1/identifier/${1} | jq -r '.repo' 2>/dev/null)
}

function get_git_rev()
{
    echo `git ls-remote kde:$repo $branch | cut -f 1`
}

function checkDownloadUptodate()
{
    local finalDestination=$1
    local result=0
    if [ "x$force" != "x-f" ]; then
        if [ -f $finalDestination ]; then
            if [ -f $versionFilePath ]; then
                fileRepoLine=`sed -n '1p' < $versionFilePath`
                if [ "$repoLine" = "$fileRepoLine" ]; then
                    rev=`get_git_rev`
                    fileRev=`sed -n '2p' < $versionFilePath`
                    if [ "$rev" = "$fileRev" ]; then
                        fileSha=`sed -n '3p' < $versionFilePath`
                        realFileSha=`sha256sum $finalDestination`
                        if [ "$fileSha" = "$realFileSha" ]; then
                            result=1
                        fi
                    fi
                fi
            fi
        fi
    fi
    return $result
}
